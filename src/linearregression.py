from comet_ml import Experiment
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt


# Set up YOUR experiment - login to comet, create new project (for new exercise)
# and copy the statet command
# or just change the name of the workspace, and the API (you can find it in the settings)
experiment = Experiment(api_key="EnterYourAPIKey",
                        project_name="exercise0", workspace="EnterGroupWorkspaceHere")


# create noisy data
np.random.seed(1337)
x = np.linspace(-1, 1, 100)
y = 2 * x + 0.3 * np.random.randn(100)
lr = 0.1  # learning rate

# setup model


class Model(object):
    def __init__(self):
        # Initialize the slope to `.1`
        self.W = tf.Variable(.1)

    def __call__(self, x):
        return self.W * x

# define optimization procedure


def objective(x, y):
    return tf.reduce_mean(tf.square(y - x))


# training
linear_model = Model()

for i in range(100):

    with tf.GradientTape() as tape:

        current_loss = objective(y, linear_model(x))
        experiment.log_metric("loss", current_loss)  # log metric to comet

        print("iteration:", i, "loss", current_loss)
        dW = tape.gradient(current_loss, linear_model.W)
        print("gradient", dW)
        linear_model.W.assign_sub(lr * dW)   # update model parameters assign_sub -> W-= lr * dW (gradient descent)


# evaluation
W_analytic = sum(y * x) / sum(x**2)  # analytic solution
print("Fitted slope", linear_model.W)
print("Analytic solution", W_analytic)


# plot data and fitted model
fig, ax = plt.subplots(1)

y_pred = linear_model(x)

ax.plot(x, y, 'bo', label='data')
ax.plot(x, y_pred, 'r-', label='model')
ax.set(xlabel='$x$', ylabel='$y$')
ax.grid()
ax.legend(loc='lower right')
fig.savefig('linearregression.png', bbox_inches='tight')

experiment.log_metric("loss", current_loss)  # log pyplot figure to comet
experiment.log_figure(figure=fig)
