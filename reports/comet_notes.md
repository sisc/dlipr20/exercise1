# Correction
## Task1
13+1/13 P
## Task 2
6/6 P
#Total : 20 / 19P


**RWTH Aachen - Deep Learning in Physics Research SS2020**

**Exercise 1 Checkerboard task**

Date: 25.4.-26.4.2020

Students:
| student            | rwth ID |
|--------------------|---------|
| Baptiste Corneglio | 411835|
| Florian Stadtmann  | 367319 |
| Johannes Wasmer    |  090800 |

# Exercise 1, Task 1 _It's Python time_

The goal for this task was to train a regression model to learn a continuous 'exclusive or' function *y(x1,x2) = 1* **IF** *(x1 > 0* **XOR** *x2 > 0)* **ELSE** *0*. Given a random
sample of points within the unit square, this function produces a checkerboard pattern, where *y=1* in quadrants II and IV, and *y=0* in quadrants I and III.

All code was run on VISPA. For each subtask, three experiments were performed and uploaded to comet.

**Note on comet experiment tagging**: all final 3\*4=12 experiments for the four subtasks 1.II, 1.IVa, 1.IVb, 1.V were run with the *same* production code (which, by extension, uses one and the same `Model` class for each subtask). Only *one* line, line 25, was adjusted for each subtask:

```python
TASK_SELECTION = 'II' # 'II', 'IVa', 'IVb', 'V' (see 'train model')
```

**Three** *final* experiments were performed per subtask (thus yielding 12 experiments), to show that the results are not deterministic. Each was tagged respectively with one of the tags *task1.II*, *task1.IVa*, *task1.IVb*, and *task1.V*.

## task1.I 
> I. Sketch the implemented network with the individual neurons and connections What is the total number of parameters? [2 P]

![](https://www.comet.ml/api/image/notes/download?imageId=1MpNzlubuAw1hX4Aw9bL3xgHs&objectId=229d3b43f3bc4395a6334bb171b59133)

The network in the original, unchanged, provided file `checkerboard.py` has one hidden layer with 4 nodes. The total
number of parameters is the total number of components of weights *#W* (the edges that connect the nodes in the figure) and biases *#b*: 

( *#W1* + *#b1* ) + ( *#W2* + *#b2* ) = (2\*4 + 4) + (4\*1 + 1) = 12 + 5 = 17.

## task1.II - task1.V
> II. Train the model and state your achieved accuracy using Comet. [1 P]

> III. Verify that TensorFlow is doing everything correctly. Extract the trained weights and biases and compute the network output and loss using pure numpy. [5 P] Bonus: Do the computation for all 100 data points without any loops. [1 P]

> IV. Change the number of neurons in the hidden layer to 8 (and to 2) and repeat the training. What do you observe? [3 P] ➢ State the different accuracies!

> V. Add an additional hidden layer of 4 neurons with a ReLU activation and retrain your model (state loss + accuracy). [2 P] ➢ Hand in loss and accuracy!

We did all that. Results and discussion see below. 

Remark on our code (see comet experiments): we adapted the `Model` class such that it's constructor takes in two lists `nodes_per_layer` and `activation_functions`. It then builds and stores the layers as three lists (weights, biases, activation functions). This is convenient, because for the iteration, forward and backward pass, and manual loss calculation, we can simply loop over the layers instead of referencing specific weights and biases explicitly. For the manual loss calculation, we avoided looping over the dataset by using `numpy` built-in functions which work on arrays: `loss_numpy = np.mean(np.square(ydata - y_numpy))`.

### Charts and plots
#### task1.II
- nodes per layer: (2, 4, 1).
- activation functions: (reLU, sigmoid).

plot: accuracy ↓

![task1.II-accuracy](https://www.comet.ml/api/image/notes/download?imageId=qh1LRktyQxIzpRszihl2FYMoS&objectId=229d3b43f3bc4395a6334bb171b59133 =700x400)

plot: loss ↓

![](https://www.comet.ml/api/image/notes/download?imageId=UJF9wLqbCQNIL3nqAGyKLQifJ&objectId=229d3b43f3bc4395a6334bb171b59133 =700x400)

plot: representative example, evaluation with test data ↓

![](https://www.comet.ml/api/image/notes/download?imageId=CBTbDaUhI3oqbW7RSHJpK6r1v&objectId=229d3b43f3bc4395a6334bb171b59133)

#### task1.IVa
- nodes per layer: (2, 8, 1).
- activation functions: (reLU, sigmoid).

plot: accuracy ↓

![](https://www.comet.ml/api/image/notes/download?imageId=M8Tzv9gJRveQz4M3BsWC2T9Kr&objectId=229d3b43f3bc4395a6334bb171b59133 =700x400)

plot: loss ↓

![](https://www.comet.ml/api/image/notes/download?imageId=WNU8B9tituyBlhJgQ5MGSOdJG&objectId=229d3b43f3bc4395a6334bb171b59133 =700x400)

plot: representative example, evaluation with test data ↓

![](https://www.comet.ml/api/image/notes/download?imageId=gT93UNCyEpjRdVQ9rgOeu9lU3&objectId=229d3b43f3bc4395a6334bb171b59133)
#### task1.IVb
- nodes per layer: (2, 2, 1).
- activation functions: (reLU, sigmoid).

plot: accuracy ↓

![](https://www.comet.ml/api/image/notes/download?imageId=TP4oefawBcApnUIK1Zh5VZ0ak&objectId=229d3b43f3bc4395a6334bb171b59133 =700x400)

plot: loss ↓

![](https://www.comet.ml/api/image/notes/download?imageId=7SQz2wQqGzreUMvOEssO29Fid&objectId=229d3b43f3bc4395a6334bb171b59133 =700x400)

plot: representative example, evaluation with test data ↓

![](https://www.comet.ml/api/image/notes/download?imageId=WBhWvgIOmRtRa5bow6G51omyV&objectId=229d3b43f3bc4395a6334bb171b59133)

#### task1.V
- nodes per layer: (2, 4, 4, 1).
- activation functions: (reLU, reLU, sigmoid). 

plot: accuracy ↓

![](https://www.comet.ml/api/image/notes/download?imageId=Ft6NSrz0u7atnKW4Y30po0ZOR&objectId=229d3b43f3bc4395a6334bb171b59133 =700x400)

plot: loss ↓

![](https://www.comet.ml/api/image/notes/download?imageId=MnThJ1gaszEbXDLurlmGGvkhb&objectId=229d3b43f3bc4395a6334bb171b59133 =700x400)

plot: representative example, evaluation with test data ↓

![](https://www.comet.ml/api/image/notes/download?imageId=sb5dH5HoSJ7Rk86tpE1HvYhST&objectId=229d3b43f3bc4395a6334bb171b59133)

### Numerical Results

| subtask                   |     1.II |    1.IVa |    1.IVb |       1.V |
|---------------------------|----------|----------|----------|-----------|
| nodes per layer           |  (2,4,1) |  (2,8,1) |  (2,2,1) | (2,4,4,1) |
| representative experiment |    2047b |    3e612 |    60e4c |     527f5 |
| loss tf                   | 0.029600 | 0.018159 | 0.115405 |  0.009528 |
| loss numpy                | 0.029599 | 0.018159 | 0.115405 |  0.009528 |
| difference(tf,numpy)      | 8.10e-07 | 5.44e-07 | 1.49e-08 |  5.49e-07 |
| accuracy tf               |    0.970 |    0.992 |    0.833 |     0.995 |
| iterations                |      2e4 |      2e4 |      2e4 |       2e4 |
| runtime                   | 00:01:12 | 00:01:11 | 00:01:16 |  00:01:45 |

### Discussion

> II. Train the model and state your achieved accuracy using Comet. [1 P]

task1.II: The achieved accuracy was around 97 %, see table above, for the representative experiment.

> III. Verify that TensorFlow is doing everything correctly. Extract the trained weights and biases and compute the network output and loss using pure numpy. [5 P] Bonus: Do the computation for all 100 data points without any loops. [1 P]

task1.III: The difference d(tf,numpy), see table above, between the two losses (tf = tensorflow, numpy = manual calculcation with numpy) is around 30 parts per million. It may be assumed that this small difference simply originates from floating-point errors, and thus verifies the model.

> IV. Change the number of neurons in the hidden layer to 8 (and to 2) and repeat the training. What do you observe? [3 P] ➢ State the different accuracies!

task1.IV: As could expected, the accuracy rises for 1.IVa eight nodes to 99.2 %, and falls to 83.3 % for two nodes in the hidden layer, see table above, for the representative experiments, respectively. 

> V. Add an additional hidden layer of 4 neurons with a ReLU activation and retrain your model (state loss + accuracy). [2 P] ➢ Hand in loss and accuracy!

task1.V: With the lowest loss of 0.0095 and the highest accuracy of 99.5 % among the representative experiments for the four different models experimented on here, see table above, this one with two hidden layers instead of one wins out. Although, it ran one and a half times as long as the others for the same iterations count. On top of that, the loss and accuracy plots above show, that it takes the model much longer to 'get off the ground': while the others start to converge almost immediately, the learning rate, i.e. gradient descent step width, in the two-hidden-layer model is almost zero throughout the first half of the runtime. Additionally, and seemingly randomly, two of the three experiments run with this model got stuck in a local minimum and only achieved around 85 % accuracy, while this happened in none of the other nine experiments with one hidden layer. So at first glance it seems that this model is also more 'brittle' or sensitive to minor disturbances. In any case, repeated runs are an absolute must when selecting deep learning models.

# Task 2: *On the playground*
## I. Trying various settings

To assure a certain reproducibility, the following configurations have been
kept constant:

Learning rate  | Activation function  | Training / test data  | Noise  | Batch size   
--|---|---|---|---
0.003  |ReLU|  50% | 0  | 20   

### Testing the neuronal networks

Different configurations of the neuronal network have been tested, running
them multiple times and waiting at least 1000 epochs to compare them.
The results are summerized in the following table. There is no logging of the convergence time, just the end result is considered, which is the test loss and training loss. A visual inspection of the resulting checkerboard will be expressed as correct classified (yes), incorrect classified (no) and partially correctly classified (partial).

| Experiment number| First hidden layers |Second hidden layers |Third hidden layers | test loss | train loss| correct classification 
--|---|---|---|---|---|---
| 0 | 0 | 0 | 0 | 0.503 | 0.494 | no 
| 1 | 1 | 0 | 0 | 0.417 | 0.360 | no 
| 2 | 2 | 0 | 0 | 0.297 | 0.235 | no 
| 3 | 3 | 0 | 0 | 0.033 | 0.002 | yes 
| 4 | 4 | 0 | 0 | 0.030 | 0.001 | yes 
| 5 | 1 | 1 | 0 | 0.441 | 0.385 | no 
| 6 | 1 | 2 | 0 | 0.414 | 0.362 | no 
| 7 | 2 | 1 | 0 | 0.315 | 0.348 | partial 
| 8 | 2 | 2 | 0 | 0.279 | 0.228 | partial 
| 9 | 2 | 3 | 0 | 0.251 | 0.268 | partial 
|10 | 2 | 4 | 0 | 0.252 | 0.261 | partial 
|11 | 3 | 2 | 0 | 0.005 | 0.000 | yes 
|12 | 3 | 3 | 0 | 0.019 | 0.001 | yes 
|13 | 4 | 2 | 0 | 0.032 | 0.000 | yes 
|14 | 4 | 3 | 0 | 0.005 | 0.000 | yes 
|15 | 4 | 4 | 0 | 0.001 | 0.000 | yes 
|16 | 1 | 1 | 1 | 0.430 | 0.390 | no 
|17 | 2 | 2 | 2 | 0.124 | 0.104 | partial 
|18 | 3 | 2 | 2 | 0.020 | 0.016 | yes 
|19 | 2 | 3 | 2 | 0.244 | 0.240 | partial 
|20 | 2 | 2 | 3 | 0.253 | 0.245 | partial 
|21 | 6 | 3 | 2 | 0.018 | 0.015 | yes 

+ partial, in this context, referse to either one or two quarters of the checkerboard correctly classified

After trying various configurations of hidden layers with varying numbers of
neurons, it seems that the smallest network guaranteeing results consists of
one hidden layer with a minimum of three neurons (experiment 3).

Although it is possible to
obtain a correct classification, this is not the case for every run of the
 network; all repetitions alter slightly the output, but in some cases,
the model starts with an initial weighting, shifting the final output away from the training data. In such a case, the network often cannot introduce changes of the weighting important ennough to make a correct classification.

## II.  Observations

As previously stated, it seems that a minimun of three neurons grouped inside one layer are necessary to get a correct result, even tho it doesn't garantees a stable output.

Also the numbers of hidden layers influence the output. At first sight, more layers seem to lead to a better overall result (smaller test loss). But it depends also on the number of neurons in each layer.
But better results can also be obtained with a smaller number of layers (e.g experiment (11) vs (18)).

Also a higher number of neurons seem to influence the convergence time of the classification positevly, but only if more neurons are in the first layer.

The best results are obtained, when using a cascade like combination of layers and neurons (e.g. experiment (11) vs (12)), meaning that the first layers hold the most neurons and the following layers hold a decreasing number of neurons. Obviously, there cannot
be a final conclusion using only the _tensorflow playground_.

Training the network with the same setting multiple times does not guarantee the same output because the initial values for the weights are choosen randomly and sometimes the network runs into sattlepoints or local minima of the loss function and gets stuck.


## III. Try additional input features: Which one is most helpful?  [2P]

The parameters from above are kept the same for a network with two hidden layers with 4 nodes in the first one and 2 in the second one. The network is trained for 500 epochs and the result as well as the speed are taken into accout. Different input features were tested with this network. No explicit values are mentioned here, because the training progress varies from run to run as explained in II and therefore every feature was tested with an average of 20-30 runs. Some training results are shown as examples to show the general behavior. Some features perform better than others:

Training with the X1 and X2 features only yields similar results to the ones mentioned above. The rectangles are not perfectly determined, but thery have straight edges and are roughly placed in the right spots:

![](https://www.comet.ml/api/image/notes/download?imageId=GsI31vQYmTeRqitfXQSNWgepP&objectId=229d3b43f3bc4395a6334bb171b59133)
![](https://www.comet.ml/api/image/notes/download?imageId=zlIXSUi4jDyVnEx4YCJhJZBI7&objectId=229d3b43f3bc4395a6334bb171b59133)
![](https://www.comet.ml/api/image/notes/download?imageId=TeqdRv8oYACVY06eatyGITuW3&objectId=229d3b43f3bc4395a6334bb171b59133)


The featues X12 and X22 cause heavy fluctuations in the outer regions, which lead to an unstable training process and bad results most the time. When good results were achieved, the X12 and X22 features were usually dropped by the network itself:

![](https://www.comet.ml/api/image/notes/download?imageId=RwveN6i4PhogyXcjlQf1kc4gZ&objectId=229d3b43f3bc4395a6334bb171b59133)
![](https://www.comet.ml/api/image/notes/download?imageId=LJqhdtFsvHeiRi5PGyx751kwa&objectId=229d3b43f3bc4395a6334bb171b59133)
![](https://www.comet.ml/api/image/notes/download?imageId=jOY0zk3EElNYlBNj0VjLjW18x&objectId=229d3b43f3bc4395a6334bb171b59133)


The features sin(X1) and sin(X2) can improve the result, but it depends on the random determined initial weights. The network runs into fale assumptions frequently, especially the previously as 'partial' marked results where only one rectangle is fitted correctly. Additionally, the sinus contributions cause higher orders to appear in the borders while the combinations of X1 and X2 keep the straight lines that the data model has.

![](https://www.comet.ml/api/image/notes/download?imageId=IOT5VNQzjmD0HJQT0jYzj10Kj&objectId=229d3b43f3bc4395a6334bb171b59133)
![](https://www.comet.ml/api/image/notes/download?imageId=NfTyP50iIFCoqzqmjR6a1Wvjd&objectId=229d3b43f3bc4395a6334bb171b59133)
![](https://www.comet.ml/api/image/notes/download?imageId=t1AOFe3nmlTRMXmQYsQIcq0uc&objectId=229d3b43f3bc4395a6334bb171b59133)


The feature X1X2 works best, but already represents a big part of the solution exept for the smooth transitions between the rectangles. By using only this feature without any hidden layers, good results are achieved. If one adds this feature to the X1 and X2 features, the best results are achieved. The network uses X1X2 mostly and only uses combinations with X1 and X2 to add sharper edges between the rectangles.

![](https://www.comet.ml/api/image/notes/download?imageId=gobfmgFFkmBhoBfUzdezNLsau&objectId=229d3b43f3bc4395a6334bb171b59133)
![](https://www.comet.ml/api/image/notes/download?imageId=0cIF0vDV8a18lFY2PlADP7Bly&objectId=229d3b43f3bc4395a6334bb171b59133)
![](https://www.comet.ml/api/image/notes/download?imageId=TE6hnMtawdWFORVpydf0g2wye&objectId=229d3b43f3bc4395a6334bb171b59133)


Overall the X12, X22 reduce the performance, sin(X1), sin(X2) can improve the performance if the network is trained multiple times and the best is result choosen, and X1X2 increases the performence, since we are adding almost the solution here.
Using only X1 and X2 mostly results in reproducing features similar to X1X2 in the second hidden layer as it is the best description for the data.












